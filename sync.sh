#!/bin/bash

echo -e "Cleaning ..."
rm .vimrc
rm -rf nvim

echo -e "Syncing ..."
mkdir nvim
cp ~/.vimrc .
cp ~/.config/nvim/ . -R

echo -e "Finished."
exit 0

#!/bin/bash

echo -e "Clean up current configs ..."
rm ~/.vimrc
rm ~/.config/nvim -rf

echo -e "Populate configs ..."
cp .vimrc ~/.vimrc
cp nvim ~/.config/. -R

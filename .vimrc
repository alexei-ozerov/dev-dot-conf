syntax enable

set number
highlight! link SignColumn LineNr
set background=dark

set backspace=indent,eol,start
colorscheme slate
set noshowmode

set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2
filetype plugin indent on
set fillchars+=vert:\
let g:NERDTreeWinSize=60

inoremap jw <Esc>
inoremap wj <Esc>

set nocompatible

let g:airline_powerline_fonts = 1

" Split Navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set encoding=utf-8

set wildmode=longest,list,full
set wildmenu

highlight clear SignColumn

" Clipboard
set clipboard+=unnamedplus

" Popup Styling
highlight NormalFloat ctermbg=darkblue
highlight Pmenu ctermbg=darkblue
highlight CocErrorFloat ctermfg=red
highlight CocFloating ctermbg=darkblue

" Initialization
let g:startify_custom_header = [
\'     =================     ===============     ===============   ========  ========  ',
\'     \\ . . . . . . .\\   //. . . . . . .\\   //. . . . . . .\\  \\. . .\\// . . //  ',
\'     ||. . ._____. . .|| ||. . ._____. . .|| ||. . ._____. . .|| || . . .\/ . . .||  ',
\'     || . .||   ||. . || || . .||   ||. . || || . .||   ||. . || ||. . . . . . . ||  ',
\'     ||. . ||   || . .|| ||. . ||   || . .|| ||. . ||   || . .|| || . | . . . . .||  ',
\'     || . .||   ||. _-|| ||-_ .||   ||. . || || . .||   ||. _-|| ||-_.|\ . . . . ||  ',
\'     ||. . ||   ||-   || ||  `-||   || . .|| ||. . ||   ||-   || ||  `|\_ . .|. .||  ',
\'     || . _||   ||    || ||    ||   ||_ . || || . _||   ||    || ||   |\ `-_/| . ||  ',
\'     ||_-  ||  .|/    || ||    \|.  || `-_|| ||_-  ||  .|/    || ||   | \  / |-_.||  ',
\'     ||    ||_-       || ||      `-_||    || ||    ||_-       || ||   | \  / |  `||  ',
\'     ||    `          || ||         `     || ||    `          || ||   | \  / |   ||  ',
\'     ||            .===  `===.         .=== .`===.         .===  /==. |  \/  |   ||  ',
\'     ||         .==    \_|-_ `===. .===    _|_   `===. .===  _-|/   `==  \/  |   ||  ',
\'     ||      .==     _-     `-_  `=     _-    `-_    `=   _-    `-_  /|  \/  |   ||  ',
\'     ||   .==     _-           `-__\._-          `-_./__-          `  |. /|  |   ||  ',
\'     ||.==     _-                                                      `  |  /==.||  ',
\'     ==     _-                                                             \/   `==  ',
\'     \   _-                                                                 `-_   /  ',
\'      `                                                                        ``    ',
\'',
\'',
\'                                      Alexei Ozerov',
\'                                alexei.ozerov.7@gmail.com',
\'',
\'',
\''
\]

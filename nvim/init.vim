set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
runtime ./plug.vim

runtime ./plug.vim
if has("unix")
  let s:uname = system("uname -s")
endif

runtime ./maps.vim

if exists("&termguicolors") && exists("&winblend")
  syntax enable
  set winblend=0
  set wildoptions=pum
  set pumblend=5
endif
